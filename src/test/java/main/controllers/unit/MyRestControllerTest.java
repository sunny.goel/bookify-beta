package main.controllers.unit;

import main.controllers.MyRestController;
import org.junit.Assert;
import org.junit.Test;

public class MyRestControllerTest {


       @Test
        public void should_return_Sample_as_name(){
            MyRestController mrc = new MyRestController();
            Assert.assertEquals("Sample" , mrc.getResponse().getName());
        }
}
