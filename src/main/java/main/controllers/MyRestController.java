package main.controllers;

import main.books.Book;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyRestController {

    @RequestMapping("/book")
    public Book getResponse(){


        return new Book("Sample");
    }
}
